<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
        .card{ width:50%;
            margin:auto;
            box-shadow:0 0 2px 3px grey;
            text-align:center;
            border-radius:10px;
            
        }
        
        .card img{ width:50%;
        
        }
        p {
            text-align:center;
            margin:10 px auto;
            font-size:30px;
            font-weight:bold;
        } 
    </style>
</head>    

    <?php

    // La variable $perso contient un chiffre entre 1 et 3. 
    // Si $perso contient 1, afficher l'image 1 ainsi que le tableau complet du premier personnage
    // Votre code contiendra les éléments qui sont dans le head (.card est une div) et vous styliserez les selecteurs
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    $perso = rand(1,3);
    
    $vador = ["Attaque" => 200, "Défense" => 120, "Force" => 100];
    $trooper = ["Attaque" => 100, "Défense" => 70, "Force" => 0];
    $r2d2 = ["Attaque" => 50, "Défense" => 20, "Force" => 0];
    
    if ($perso==1) {

        echo "<div class='card'> <img src='pop1.jpg'></div>";
    foreach ($vador as $clef =>$vador1){

        echo "<p> ".$clef. " = " .$vador1. "</p>";
    }

    }elseif ($perso==2) {

        echo "<div class='card'> <img src='pop2.jpg'></div>";
    
    foreach ($trooper as $clef =>$trooper1){

        echo "<p> ".$clef. " = " .$trooper1. "</p>";
    }

    }else {

        echo "<div class='card'> <img src='pop3.jpg'></div>";
        
     foreach ($r2d2 as $clef =>$r2d22){

        echo "<p> ".$clef. " = " .$r2d22. "</p>";
    }
}
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>