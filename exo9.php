<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Créer un tableau associatif pour la recette des crêpes. Associez les ingrédients à leur quantités. 
    // Afficher ensuite la liste des ingrédients compléte. 
    // Farine, sel, huile, sucre, lait, oeufs, goutte et le temps de repos de la pâte. 
   
    
    ?>
    <h1>Recette des crêpes</h1>
    <!-- écrire le code après ce commentaire -->
    <?php

    $tableau = [ 'lait' => '1 litre',
                     'farine' => '400 grammes',
                     'oeufs' => 8, 
                     'sucre' => '120 grammes',
                     'sel' => '1 pincée',
                     'beurre' => '40 grammes',
                     'biere' => '60cl',
                     'vanille' => '2 gousses',
                     'temps de repos' => '2 h' ];


    foreach ($tableau as $clef=>$valeur){ 

        echo $clef. ':' .$valeur.'<br>';}
    
    
    ?>
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>
